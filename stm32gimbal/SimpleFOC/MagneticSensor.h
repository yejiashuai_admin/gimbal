#ifndef MAGNETICSENSOR_LIB_H
#define MAGNETICSENSOR_LIB_H

/******************************************************************************/
extern long  cpr;
extern float full_rotation_offset0;
extern long  angle_data_prev0;
extern unsigned long velocity_calc_timestamp0;
extern float angle_prev0;
extern float full_rotation_offset;
extern long  angle_data_prev;
extern unsigned long velocity_calc_timestamp;
extern float angle_prev;
/******************************************************************************/
void MagneticSensor_Init0(void);
float getAngle0(void);
float getVelocity0(void);
void MagneticSensor_Init(void);
float getAngle(void);
float getVelocity(void);
/******************************************************************************/

#endif
